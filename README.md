# FastConverter
Small program I was using for **common unit conversions** I often did. You can **define own math expressions** with description in csv file. I had program linked to keyboard shortcut. It started I started typing, result was immediately visible, I closed program by Esc and rewrote remembered result to web. It was fast and functional.

##Usage
You have to set conversions csv file. Delimiter is semicolon.

Example *conversions.csv*
~~~~
lbs = kg; x * 0.45359237
inches = cm; x * 2.54
feet = m; x * 0.3048
<html>CFM = m<sup>3</sup>/h</html>; x*1.6990107955
~~~~
First part is description of math expression, you can even use HTML like in last row. Second part is math expression with defined variable x.

With proper conversions file you can run program by:
~~~~
FastConverter conversions.csv
~~~~
So the only parameter is conversions file. Appears this (but probably in English):

![screenshot](http://karelik.wz.cz/images/107.png)

You can just start typing number and all expressions are refreshed. End it by Esc.

Enjoy it!