package cz.ulman.fastconverter;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.text.NumberFormat;
import java.util.ResourceBundle;

class FormulaRow {
	private static final int MAX_FRACTION_DIGITS = 5;

	private final NumberFormat nf = NumberFormat.getInstance();

	private final Expression expression;
	private final TextField textField;
	private final Label label;


	FormulaRow(String[] lineRecords, ResourceBundle msg) {
		// creates gui objects connected with formula
		label = new Label(lineRecords[0]);
		expression = new ExpressionBuilder(lineRecords[1]).variable("x").build();

		// result text field
		textField = new TextField();
		textField.setText(msg.getString("not.available"));
		textField.setEditable(false);

		// initial actions
		nf.setMaximumFractionDigits(MAX_FRACTION_DIGITS);
	}

	/**
	 * Calculates result of formula with changed variable value.
	 *
	 * @param number
	 * @return
	 */
	private String getResult(double number) {
		expression.setVariable("x", number);
		return nf.format(expression.evaluate());
	}

	private void update(double number) {
		textField.setText(getResult(number));
	}

	private void update(String value) {
		textField.setText(value);
	}

	/**
	 * Universal update method, chooses method between result or N/A string.
	 *
	 * @param object
	 */
	void update(Object object) {
		if (object instanceof Double) update((double) object);
		else if (object instanceof String) update((String) object);
	}

	Label getLabel() {
		return label;
	}

	TextField getTextField() {
		return textField;
	}
}
