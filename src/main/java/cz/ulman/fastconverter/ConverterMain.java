package cz.ulman.fastconverter;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.simpleflatmapper.csv.*;
import org.simpleflatmapper.util.*;

import java.io.*;
import java.util.*;

public class ConverterMain extends Application {
	private static final ResourceBundle MSG = ResourceBundle.getBundle("msg");
	private final List<FormulaRow> formulaRowList = new LinkedList<>();

	public void start(Stage stage) throws IOException {
		readCsvConfig(getParameters().getRaw().get(0));
		initGraphics(stage);
	}

	private void readCsvConfig(String filename) throws IOException {
		try (CloseableIterator<String[]> it = CsvParser.iterator(new File(filename))) {
			while (it.hasNext()) {
				formulaRowList.add(new FormulaRow(it.next(), MSG));
			}
		}
	}

	private void initGraphics(Stage stage) {
		stage.setTitle(MSG.getString("title"));
		final GridPane gridPane = new GridPane();
		final Label lbVal = new Label(MSG.getString("value"));
		final TextField tfValue = new TextField();
		tfValue.setOnKeyPressed(event -> {
			// Allows only fast close of program by ESC button
			switch (event.getCode()) {
				case ESCAPE:
					Platform.exit();
			}
		});
		tfValue.textProperty().addListener((observable, oldValue, newValue) -> {
			// Immediately after change of text field refreshes all rows with new results.
			final String numberStr = newValue.trim().replace(",", ".");// tolerate czech comma too
			Object targetVal;
			try {
				targetVal = Double.parseDouble(numberStr);
			} catch (Exception e){
				targetVal = MSG.getString("not.available");
			}
			for (FormulaRow formulaRow : formulaRowList) {
				formulaRow.update(targetVal);
			}
		});
		int row = 0;
		gridPane.add(lbVal, 0, row);
		gridPane.add(tfValue, 1, row++);
		for (FormulaRow formulaRow : formulaRowList) {
			gridPane.add(formulaRow.getLabel(), 0, row);
			gridPane.add(formulaRow.getTextField(), 1, row++);
		}

		final Group root = new Group();
		root.getChildren().add(gridPane);
		final Scene scene = new Scene(root);
		stage.setResizable(false);
		stage.setScene(scene);
		stage.sizeToScene();
		stage.show();
	}


	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println(MSG.getString("usage"));
			Platform.exit();
		}
		launch(args);
	}
}
